import numpy as np
import biorbd
import bioviz
from bioviz.biorbd_vtk import VtkModel, VtkWindow, Mesh

# a bit of statistics
import time

# polytope algorithm
from pycapacity.human import force_polytope
from pycapacity.human import torque_to_muscle_force

# Load a predefined model
model = biorbd.Model("pyomeca_models/MOBL_ARMS_fixed_33.bioMod")
# model = biorbd.Model("pyomeca_models/BrasComplet.bioMod")

# get the number of dof and muslces
nq = model.nbQ()
nb_mus = model.nbMuscles()

# Animate the results if biorbd viz is installed
b = bioviz.Viz(loaded_model=model,background_color=(1,1,1),show_local_ref_frame=False, show_global_ref_frame=False, show_markers=True,show_global_center_of_mass=False,show_segments_center_of_mass=False, show_wrappings=False)
# define the meshes for the polytope - without robot
vtkMeshView = VtkModel(b.vtk_window, patch_color=[[0,0.5,0.8]],mesh_opacity=0.5)
vtkMeshView1 = VtkModel(b.vtk_window, patch_color=[[0,0.5,0.8]],mesh_opacity=0.8, force_wireframe=True)

b.set_q([0.0,1.4237,-1.256,1.8218,0.0,0.0,0.0])
while b.vtk_window.is_active:
    Q = b.Q
    model.updateMuscles(Q, True)
    model.UpdateKinematicsCustom(Q, np.zeros(nq), np.zeros(nq))

    F_max = []
    F_min = []
    for i in range(nb_mus):
        F_max.append(model.muscle(i).characteristics().forceIsoMax())
        #F_min.append(0)
        a = biorbd.HillThelenType(model.muscle(i)).FlPE()
        # print(a,biorbd.HillThelenType(model.muscle(i)).FlCE())
        if a > 1:
            a = 0.1
        elif a < 0:
            a = 0
        F_min.append(a*F_max[-1])

    start = time.time()
    N = -model.musclesLengthJacobian(Q).to_array().T
    J = model.markersJacobian(Q, False, False)[-1].to_array()
    print("time", time.time() - start)

    # Proceed with the inverse dynamics
    Tau_grav = model.InverseDynamics(Q, np.zeros(nq), np.zeros(nq))
    
    start = time.time()
    f_vert, H, d, faces = force_polytope(J, N, F_min, F_max, 10, -Tau_grav.to_array())
    print("time", time.time() - start)

    ## display polytope in the bioviz
    f_vert_show = np.vstack((f_vert[0,:],f_vert[1,:],f_vert[2,:]))/2000
    f_vert_show = f_vert_show + model.markers(Q)[model.nbMarkers()-1].to_array().reshape(3,1)
    s = f_vert_show.shape
    vert = f_vert_show.reshape(s[0],s[1],1)

    # plot polytope (blue) - with the robot
    meshes = []
    meshes.append(Mesh(vertex=vert, triangles=faces.T))
    vtkMeshView.update_mesh(meshes)
    vtkMeshView1.update_mesh(meshes)

    # update visualisation
    b.update()

