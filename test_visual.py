import bioviz
import biorbd
import numpy as np

# Load a predefined model
# model = biorbd.Model("pyomeca_models/MOBL_ARMS_fixed_33.bioMod")
# model = biorbd.Model("pyomeca_models/BrasComplet.bioMod")
# model = biorbd.Model("pyomeca_models/BrasViolon.bioMod")
# model = biorbd.Model("pyomeca_models/arm26.bioMod")
model = biorbd.Model("pyomeca_models/BrasCompletIMUS.bioMod")

# get some random data
nq = model.nbQ()
nb_mus = model.nbMuscles()
print("dof number: ",nq)
print("muscle number: ",nb_mus)


# a bit of animation
q_recons = np.zeros((model.nbQ(), 100))
for i in range(1,100):
    # increment all joint values by -0.003
    q_recons[:, i] = q_recons[:, i-1]-0.003 

# double duration of the animation
q_recons = np.hstack((q_recons,np.flip(q_recons)))

# visualize
b = bioviz.Viz(loaded_model=model)
# b.load_movement(q_recons)
b.exec()